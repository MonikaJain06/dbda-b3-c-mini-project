#ifndef _LIBRARY_H
#define _LIBRARY_H

struct user {
    int id;
    char name;
    char email;
    int phoneNo;
    char password;
    char role;

};

struct books
{
    int id;
    char name;
    char author;
    char subject;
    int price;
    int isbn;
    
    
};

struct bookcopies
{
    int id;
    int bookId;
    int rack;
    char status;
};

struct issueRecord
{
    int id;
    int copyId;
    int memberId;
    int fine_amount;
    //issue_date
    //return_date
    //return_duedate
};

struct payments
{
    int id;
    int userId;
    int amount;
    char type;
    //transaction_time;
    //nextpayment_duedate;
};




#endif



